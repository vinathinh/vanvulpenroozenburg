(function($, exports) {
	'use strict';

	var GoogleMap = exports.GoogleMap = function(options) {
		this.map = null;
		this.markers = [];
		this.bounds = null;
		this.info = null;
		this.options = $.extend({
			container: '#google-map',
			mapContainer: null,
			map: {}
		}, options || {});
		this.$container = null;
	};

	$.extend(GoogleMap.prototype, {
		load: function() {
			if (window.realworksInitMap) return;

			window.realworksInitMap = $.proxy(this.init, this);

			if (typeof google === 'undefined' || typeof google.maps === 'undefined') {
				var script = document.createElement('script');
				script.type = 'text/javascript';
				script.src = '//maps.googleapis.com/maps/api/js?key=AIzaSyAIIUzGc-kvIqKLiqo9ZmeLxjwfuSLn8A4&callback=realworksInitMap';
				document.body.appendChild(script);
			} else {
				this.init();
			}
		},

		init: function() {
			var self = this;

			this.$container = $(this.options.container);
			if (this.$container.length === 0) return;

			var mapEl = this.options.mapContainer ? document.getElementById(this.options.mapContainer) : this.$container[0];

			this.map = new google.maps.Map(mapEl, this.mapOptions());

			// Close current info window when map is clicked
			google.maps.event.addListener(this.map, 'click', function() {
				self.setInfo(null);
			});

			this.addObservers();
			this.initData();
		},

		mapOptions: function() {
			// Delayed since Maps API may not have been loaded
			return $.extend({
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}, this.options.map);
		},

		initData: function() {
			// override
		},

		detach: function() {
			if (!this.map) return false;
			this.$container.detach();
		},

		attach: function() {
			if (!this.map) return false;
			$(this.options.container).replaceWith(this.$container);
		},

		getBoundsZoomLevel: function(bounds) {
			var WORLD_DIM = { height: 256, width: 256 };
			var ZOOM_MAX = 21;

			function latRad(lat) {
				var sin = Math.sin(lat * Math.PI / 180);
				var radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
				return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
			}

			function zoom(mapPx, worldPx, fraction) {
				return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
			}

			var ne = bounds.getNorthEast();
			var sw = bounds.getSouthWest();

			var latFraction = (latRad(ne.lat()) - latRad(sw.lat())) / Math.PI;

			var lngDiff = ne.lng() - sw.lng();
			var lngFraction = (lngDiff < 0 ? lngDiff + 360 : lngDiff) / 360;

			var latZoom = zoom(0.9 * this.$container.height(), WORLD_DIM.height, latFraction);
			var lngZoom = zoom(0.9 * this.$container.width(), WORLD_DIM.width, lngFraction);

			return Math.min(latZoom, lngZoom, ZOOM_MAX);
		},

		fit: function() {
			if (!this.map) return false;
			google.maps.event.trigger(this.map, 'resize');

			var zoom = this.getBoundsZoomLevel(this.bounds);

			// Fitting to the same zoom level does not animate, so manually use a panning operation
			if (zoom === this.map.getZoom()) {
				this.map.panTo(this.bounds.getCenter());
			} else {
				this.zoomGraduallyTo(zoom);
			}
		},

		zoomGraduallyTo: function(zoom) {
			var self = this;
			var difference = zoom - this.map.getZoom();

			// When more than two zoom levels need to be changed, there won't be an animation. Zoom into the
			// target so that the remaining zoom distance will be two, then perform the final fitting operation.
			if (Math.abs(difference) > 2) {
				setTimeout(function() {
					self.zoomGraduallyTo(zoom);
				}, 100);
				this.map.setZoom(zoom - (difference > 0 ? 2 : -2));
			} else {
				google.maps.event.addListenerOnce(this.map, 'bounds_changed', function() {
					if (this.getZoom() > 16) this.setZoom(16);
				});
				this.map.fitBounds(this.bounds);
			}
		},

		setInfo: function(info) {
			if (this.info) this.info.close();

			this.info = info;
		},

		addObject: function(object) {
			var position = new google.maps.LatLng(object.latitude, object.longitude);
			var marker = new google.maps.Marker({ map: this.map, position: position });
			var info = new google.maps.InfoWindow({ content: object.template });

			return {
				position: position,
				marker: marker,
				info: info
			};
		},

		addObservers: function() {

		}
	});

	// --------------------------------------------------------------------------------------------

	var MultiMap = exports.MultiMap = function(options) {
		GoogleMap.apply(this, arguments);

		this.objects = options.objects || [];
	};
	MultiMap.prototype = new GoogleMap();

	$.extend(MultiMap.prototype, {
		constructor: MultiMap,

		initData: function() {
			this.updateMarkers();
		},

		updateMarkers: function() {
			if (!this.map) return false;
			if (!this.hasMarkers()) return false;

			// Clear current markers
			$.each(this.markers, function() {
				this.setMap(null);
			});

			this.markers = [];
			this.bounds = new google.maps.LatLngBounds;

			this.setInfo(null);
			this.addObjects();
			this.fit();
		},

		hasMarkers: function() {
			this.$container.toggle(this.objects.length > 0);

			return this.objects.length > 0;
		},

		addObjects: function() {
			var self = this;

			$.each(this.objects, function(item) {
				var object = self.addObject(this);

				google.maps.event.addListener(object.marker, 'click', function() {
					self.setInfo(object.info);
					object.info.open(self.map, object.marker);
				});

				self.markers.push(object.marker);
				self.bounds.extend(object.position);
			});
		},

		live: function(form, options) {
			var self = this;

			options = $.extend({
				type: 'all',
				detach: false,
			}, options || {});

			if (options.detach) {
				form.on('before:replace', $.proxy(this.detach, this));
				form.on('after:replace', $.proxy(this.attach, this));
			}

			form.on('request:data', function(e, data, request) {
				if (!request.paged || options.type === 'paged') data.__maps = options.type;
			});

			form.on('load', function() {
				self.updateMarkers();
			})

			form.on('updated', function(e, data) {
				if (data.maps) {
					self.objects = data.maps;
					self.updateMarkers();
				}
			});
		}
	});

	// --------------------------------------------------------------------------------------------

	var SingleMap = exports.SingleMap = function(options) {
		GoogleMap.apply(this, arguments);
	};
	SingleMap.prototype = new GoogleMap();

	$.extend(SingleMap.prototype, {
		constructor: SingleMap,

		initData: function() {
			this.setPosition();
			this.loadStreetView();
		},

		fit: function() {
			if (!this.map) return false;

			google.maps.event.trigger(this.map, 'resize');
			if (this.streetview) google.maps.event.trigger(this.streetview, 'resize');

			this.map.setCenter(this.position);
			this.map.setZoom(16);
		},

		setInfo: function(info) {
			this.info.close();
		},

		setPosition: function() {
			if (!this.map) return false;

			var self = this;
			var object = this.addObject(this.options.data);

			this.position = object.position;
			this.marker = object.marker;
			this.info = object.info;

			google.maps.event.addListener(object.marker, 'click', function() {
				self.info.open(self.map, object.marker);
			});

			this.fit();
		},

		loadStreetView: function() {
			var container = document.getElementById('google-streetview');
			if (!container) return;

			this.streetview = new google.maps.StreetViewPanorama(container, {
				position: this.position
			});
			this.map.setStreetView(this.streetview);
		}
	});
})(jQuery, this);
