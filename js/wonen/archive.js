jQuery(function($) {
	'use strict';

	var form = new CustomPost.LiveForm;

	// Stylize selects
	form.on('load', function() {
		$('select.stylized-auto-width').selectBoxIt();
		$('select.stylized').selectBoxIt({
			autoWidth: false
		});
	});

	// Show radius field only when a single city is selected
	form.on('load', function() {
		if ($('[name=plaats], [name="plaats[]"]').serializeArray().length === 1) {
			$('.search-field.radius').show();
		} else {
			$('.search-field.radius').hide();
		}
	});

	// Set a data attribute indicating how many items are checked
	// Reference from css i.e. to show only the one active item
	form.on('load refresh', function() {
		$('.track-selected').each(function() {
			$(this).attr('data-selected', $(this).find(':checkbox, :radio').serializeArray().length);
		});
	});

	// Clear selected items of the search field
	$(document).on('click', '.clear-selected', function(e) {
		e.preventDefault();
		$(this).parents('.search-field').find(':checkbox, :radio').prop('checked', false);
		form.refresh();
	});

	// Add active class to labels whose checkbox/radio is checked
	form.on('load refresh', function() {
		$('label :checkbox, label :radio').each(function() {
			$(this).parents('label')[$(this).is(':checked') ? 'addClass' : 'removeClass']('active');
		});
	});

	// Load show more links
	form.on('load', function() {
		$('.show-more').each(function() {
			var $this = $(this), data = $this.data();

			new CustomPost.ShowMore({
				container: $this,
				name: data.name,
				moreText: data.more || 'Meer...',
				lessText: data.less || 'Minder',
				defaultAmount: 'amount' in data ? data.amount : 0
			});
		});
	});

	// Load sliders
	form.on('load', function() {
		$('.search-slider').each(function() {
			var $this = $(this), data = $this.data();

			new CustomPost.Slider({
				container: $this,
				type: data.type || 'min',
				emptyLabel: data.emptyLabel,
				optionLabelAttribute: data.label,
				sliderOptions: data.options || {}
			});
		});

		$('.search-range-slider').each(function() {
			var $this = $(this), data = $this.data();

			new CustomPost.RangeSlider({
				container: $this,
				minLabel: data.minLabel,
				maxLabel: data.maxLabel,
				optionLabelAttribute: data.label,
				sliderOptions: data.options || {}
			});
		});
	});

	// Scroll to top on page change
	form.on('change:page', function() {
		var to = $('#entity-results').offset().top;
		var from = $(window).scrollTop();

		if (from > to) {
			$('html, body').animate({ scrollTop: to }, 300);
		}
	});

	// Slide in infinite scroll results
	form.on('before:append', function(form, item) {
		$(item).hide();
	});

	form.on('after:append', function(form, item) {
		$(item).slideDown();
	});

	// Remove pagination when infinite results ended
	form.on('infinite:end', function() {
		$('.pagination').remove();
	});

	// Load Google Map
 
    var map = new MultiMap({ objects: Wonen.maps });
	map.live(form, { type: 'all' });
	map.load();

	form.init();
 


	// Add JS helper class to document body
	$(document.body).addClass('has-js');

	var currentDisplay;

	// Support for multiple display types
	function setDisplayType(display) {
		if (display === 'map') map.load();
		$('.search-content').removeClass('grid list map').addClass(currentDisplay = display);
		document.cookie = 'display='+display;
	}

	// Switch display type when their links are clicked
	$(document).on('click', '.search-display', function(e) {
		e.preventDefault();
		setDisplayType($(this).data('display'));
	});

	setDisplayType(document.cookie.replace(/(?:(?:^|.*;\s*)display\s*\=\s*([^;]*).*$)|^.*$/, '$1') || 'list');

	// Infinite scrolling
	var scrolled = false, $window = $(window);
	$window.scroll(function() { scrolled = true; });

	setInterval(function() {
		if (scrolled && currentDisplay !== 'map' && !form.isLoading() && $window.scrollTop() + $window.height() >= form.$el.offset().top + form.$el.height()) {
			$('.infinite-results a, a.infinite-results').click();
		}
		scrolled = false;
	}, 100);

});
