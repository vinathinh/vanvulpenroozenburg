jQuery(function($) {
	'use strict';

	if (Wonen.map) {
		var map = new SingleMap({ data: Wonen.map });

		map.load();
	}
});
