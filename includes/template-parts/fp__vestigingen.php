<!-- <?php 
	$rows = get_field('vestiging', 'option');
	$first_row = $rows[0];
	$naam = $first_row['naam'];
	$straat = $first_row['straat'];
	$postcode = $first_row['postcode'];
	$plaats = $first_row['plaats'];
 ?>

<ul>
	<li><?php echo $naam; ?></li>
	<li><?php echo $straat; ?></li>
	<li><?php echo $postcode; ?><?php echo $plaats; ?></li>
</ul> -->

<section>

	<?php if(get_field('vestiging', 'option')): ?>

		<div class="vestigingen">
		<h2><?php the_sub_field('titel') ?></h2>
		<?php $i = 1; while(has_sub_field('vestiging', 'option')): ?>
		<div class="container center vestigingen--<?php echo $i; ?>">
			<div class="vestigingen__map col-6 left">
				<div id="map">
					<div id="marker" data-lat="52.215389" data-lng="5.167606"></div>
				</div>
			</div>
			<div class="vestigingen__gegevens col-6 left">
				<h3><?php the_sub_field('naam') ?></h3>
				<ul> 
					<li><?php the_sub_field('straat') ?></li>
					<li><?php the_sub_field('postcode') ?> <?php the_sub_field('plaats') ?></li>
					<li>Tel. <?php the_sub_field('telefoonnummer') ?></li>
				</ul>
				<a href="#" class="vestigingen__gegevens--afspraak">Maak direct een afspraak</a>
			</div>
			<div class="clearfix"></div>
		</div>
		<?php $i++; endwhile; ?>

		</div>

	<?php endif; ?>

</section>


<?php $maps = $first_row['kaart']; ?>
    <script>
      var map;
      function initMap() {
      	var myLatLng = {lat: 52.215389, lng: 5.167606};

        var map = new google.maps.Map(document.getElementById('map'), {
          center: myLatLng,
          zoom: 16,
          scrollwheel: false,
          mapTypeControl: false,
          disableDefaultUI: true
        });

        var marker = new google.maps.Marker({
		    position: myLatLng,
		    map: map,
		    title: 'Hello World!'
		  });

      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBomWnlLQsuCPhFCezufeOvXRKgvlKimW8&callback=initMap"
    async defer></script>
