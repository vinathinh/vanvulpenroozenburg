<?php

$args = array(
	'post_type' => 'post',
	'posts_per_page' => 3 
);
$news_posts = new WP_Query($args);

if($news_posts->have_posts()) : $counter = 0; ?>


<?php while($news_posts->have_posts()): $news_posts->the_post(); ?>
	<?php if($counter == 0): ?>

    <?php include 'includes/fp__nieuws--lg.php'; ?>

	<?php else: ?>
		<?php include 'includes/fp__nieuws--sm.php'; ?>
	<?php endif; ?>

	<?php $counter++; ?>

	<?php endwhile;	?>

<?php endif; wp_reset_postdata(); ?>