<section class="fp__intro">
	<div class="fp__intro--middle">
		<h1><?php the_sub_field('titel') ?></h1>
		<h2><?php the_sub_field('subtitel') ?></h2>
		<p><?php the_sub_field('onderschrift') ?></p>
	</div>
</section>

