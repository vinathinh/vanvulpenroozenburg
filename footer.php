	<footer>
		<div class="container center">
			<div class="col-2 left">
				<p>Overzicht</p>
				<?php
					wp_nav_menu( array(
				        'theme_location' => 'footer',
				        'menu_class'     => 'footer-menu',
			    	) ); 
		    	?>
	    	</div>
	    	<div class="col-4 left">
	    		<p>Onze vestigingen</p>
	    		<?php if(get_field('vestiging', 'option')): ?>

				<?php $i = 1; while(has_sub_field('vestiging', 'option')): ?>
				<div class="">

					<div class="">
						
						<ul> 
							<li><?php the_sub_field('naam') ?></li>
							<li><?php the_sub_field('straat') ?></li>
							<li><?php the_sub_field('postcode') ?> <?php the_sub_field('plaats') ?></li>
							<li>Tel. <?php the_sub_field('telefoonnummer') ?></li>
						</ul> 
					</div>
					<div class="clearfix"></div>
				</div>
				<?php $i++; endwhile; ?>

			</div>

			<?php endif; ?> 
	    	<div class="col-4 left">
	    		logo logo logo
	    	</div>
		</div>
		<div class="copyright">Van Vulpen & Roozenburg makelaars | <?php echo date("Y"); ?></div>
	</footer>
<script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
<script src="https://npmcdn.com/flickity@2/dist/flickity.pkgd.js"></script>
<?php wp_footer(); ?>

</body>
</html>