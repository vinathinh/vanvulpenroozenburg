<article class="woning col-4 left">
	<div class="item">
		<a href="<?php the_permalink(); ?>" class="thumbnail">
			<?php the_post_thumbnail('medium'); ?>
		</a>

		<div class="item-content">
			<div>
				<?php the_title('<h1><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h1>'); ?>
			</div>

			<div class="entry-content hidden-sm">
				<?= $woning->listing('prijs', 'woning.gebruiksoppervlakteWoonfunctie', 'woning.aantalKamers')->item('<div>#{value}</div>'); ?>
			</div>
		</div>

		<a href="<?php the_permalink(); ?>" class="overlay"></a>
	</div>
</article>
