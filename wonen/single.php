<?php get_header(); ?>
<?php the_post(); ?>

<?php wp_enqueue_script('wonen-single') ?>
<?php wp_localize_script('wonen-single', 'Wonen', array(
	'map' => $woning->map(),
	//'nearby' => $woning->nearby('10km')->all()->maps(),
)); ?>
<div class="single__breadcrumbs">
	<div class="container center">BREADCRUMBS</div>
</div>
<div id="primary" class="site-content container center">
	<div id="content" role="main">
	
	<div class="col-12"><a href="/vanvulpenroozenburg/woningen" class="single__terug">< TERUG NAAR DE RESULTATEN</a></div>
	<div class="col-6 left single__title">
	
	<h2><?= $woning->adres->value(); ?></h2>
	<?= $woning->listing('adresNederlands.postcode', 'plaats' )->before('<div class="single__title--postcode">')->after('</div>')->item('<div>#{value}</div>'); ?>
	</div>
	<div class="col-6 left single__prijs"><?= $woning->prijs->render($currency = null, $suffix = null, $decimals = null); ?></div>
	<div class="clearfix"></div>

	<!-- <?= $woning->koopmengvorm->value(); ?> -->

	
	<!-- <?= $woning->listing('woning.appartement.soort', 'woning.woonhuis.soort', 'status' ); ?> -->

		<article class="woning">
			<?php if ($woning->fotos->has()): ?>
				<!-- <h2>Foto's</h2> -->
				<div class="carousel-container">
					<div class="carousel carousel-main is-hidden">
						<?php foreach ($woning->fotos as $foto): ?>
							<?php $photo = wp_get_attachment_image_src($foto->ID, 'large'); ?>
							<div class="carousel-cell" style="background-image: url(<?= $photo[0] ?>);">
							
							</div>
						<?php endforeach; ?>
					</div>

					<div class="carousel-nav"> 
						<?php foreach ($woning->fotos as $foto): ?>
							<?php $photo = wp_get_attachment_image_src($foto->ID, 'thumbnail'); ?>

							<div class="carousel-cell" style="background-image: url(<?= $photo[0] ?>);">
							
							
							
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endif; ?>
			
			<!-- <header> -->
				<!-- <?php the_title('<h1>', '</h1>'); ?> -->
			<!-- </header> -->
			<div class="row-sm">
				<div class="col-8 left padding-sm">
					<div class="single__omschrijving">
						<h1 class="single__omschrijving--title">Omschrijving</h1>
						<div class="single__omschrijving--content"><?php the_content(); ?></div>
						<p class="single__omschrijving--toggle">+ MEER OMSCHRIJVING</p>
					</div>

				 

				<h3>KENMERKEN</h3>

				<?= $woning->listing(array(
					'woning.bouwjaar', 
					'bouwvorm', 
					'woning.gebruiksoppervlakteWoonfunctie' => 'Oppervlakte', 
					'woning.aantalKamers' 
				))->before('<div class="kenmerkenlijst">')->after('</div>')->item('<div><div class="col-4 left">#{label}</div> <div class="col-8 left">#{value}</div></div>'); ?>
				toggle
				
				
			<div class="kenmerken__closed">
				<h3>Overdracht</h3>
				<?= $woning->listing(array(
					'status', 
					'prijs' => 'Prijs'
				))->before('<div class="kenmerkenlijst">')->after('</div>')->item('<div><div class="col-4 left">#{label}</div> <div class="col-8 left">#{value}</div></div>'); ?>

				<h3>Bouwvorm</h3>
				<?= $woning->listing(array(
					'woning.woonhuis.kenmerk',
					'woning.woonhuis.soort',
					'woning.woonhuis.type', 
					'woning.bouwjaar', 
					'bouwvorm',
					'woning.liggingen' 
				))->before('<div class="kenmerkenlijst">')->after('</div>')->item('<div><div class="col-4 left">#{label}</div> <div class="col-8 left">#{value}</div></div>'); ?>

				<h3>Indeling</h3>
				<?= $woning->listing(array(
					'woning.gebruiksoppervlakteWoonfunctie' => 'Woonoppervlakte', 
					'woning.perceelOppervlakte',
					'woning.inhoud', 
					'woning.aantalKamers',
					'woning.aantalSlaapkamers',
					'woning.appartement.aantalWoonlagen',
					'woning.aantalVerdiepingen',
					'woning.dakToelichting'
				))->before('<div class="kenmerkenlijst">')->after('</div>')->item('<div><div class="col-4 left">#{label}</div> <div class="col-8 left">#{value}</div></div>'); ?>

				<h3Energie</h3>
				<?= $woning->listing(array(
					'woning.energieklasse', 
					'woning.energielabelEinddatum',
					'woning.installatie.soortenVerwarming',
					'woning.installatie.CVKetelType',
					'woning.installatie.CVKetelBouwjaar',
					'woning.isolatievormen', 
					'woning.installatie.CVKetelCombiketel',
					'woning.installatie.CVKetelBrandstof',
					'woning.installatie.CVKetelEigendom'
				))->before('<div class="kenmerkenlijst">')->after('</div>')->item('<div><div class="col-4 left">#{label}</div> <div class="col-8 left">#{value}</div></div>'); ?>
				
				<h3>Buitenruimte</h3>
				<?= $woning->listing(array(
					'woning.tuin.tuintypen', 
					'woning.tuin.kwaliteit',
					'woning.tuin.totaleOppervlakte',
					'woning.hoofdtuin.type',
					'woning.hoofdtuin.positie',
					'woning.hoofdtuin.achterom',
					'woning.hoofdtuin.lengte',
					'woning.hoofdtuin.breedte',
					'woning.hoofdtuin.oppervlakte'
				))->before('<div class="kenmerkenlijst">')->after('</div>')->item('<div><div class="col-4 left">#{label}</div> <div class="col-8 left">#{value}</div></div>'); ?>

				<h3>Parkeergelegenheid</h3>
				<?= $woning->listing(array(
					'woning.garage.soorten', 
					'woning.garage.voorzieningen',
					'woning.garage.totaalAantalGarages',
					'woning.garage.capaciteit',
					'woning.parkeerFaciliteiten',
					'woning.parkerenToelichting',
					'woning.parkerenToelichting',
					'parkeerplaats.capaciteit',
					'parkeerplaats.toelichting'
				))->before('<div class="kenmerkenlijst">')->after('</div>')->item('<div><div class="col-4 left">#{label}</div> <div class="col-8 left">#{value}</div></div>'); ?>

				<h3>Overig</h3>
				<?= $woning->listing(array(
					'woning.permanenteBewoning', 
					'woning.onderhoudBinnen.waardering',
					'woning.onderhoudBuiten.waardering',
					'woning.huidigGebruik',
					'woning.huidigeBestemming'
				))->before('<div class="kenmerkenlijst">')->after('</div>')->item('<div><div class="col-4 left">#{label}</div> <div class="col-8 left">#{value}</div></div>'); ?>

				<h3>Voorzieningen</h3>
				<?= $woning->listing(array(
					'woning.voorzieningen'
				))->before('<div class="kenmerkenlijst">')->after('</div>')->item('<div><div class="col-4 left">#{label}</div> <div class="col-8 left">#{value}</div></div>'); ?>
			</div>



				</div>

				<div class="col-4 left padding-sm">
					<div class="single__sidebar">
						<button class="btn-blue">NEEM CONTACT MET ONS OP</button>
						<a href="mailto:?subject=<?php echo get_the_title(); ?>&body=<?php echo get_permalink(); ?>"><button class="btn-grey">DOORSTUREN</button></a>

						<div class="single__sidebar--phone"><span class="icon-phone"></span><a href="tel:035646228">035-646228</a></div>
						<div class="single__sidebar--email"><span class="icon-email"></span><a href="mailto:info@vanvulpenroozenburg.nl">info@vanvulpenroozenburg.nl</a></div>

						<div class="single__sidebar--print"><span class="icon-print"></span><a href="javascript:window.print()">pagina afdrukken</a></div>

						<div class="socialicons">
							<a href="https://www.facebook.com/share.php?u=<?php echo get_permalink(); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/facebook-logo.svg" alt=""></a>
							<a href="http://twitter.com/share?url=<?php echo get_permalink(); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/twitter-logo.svg" alt=""></a>
							<a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo get_permalink(); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/icons/linkedin-logo.svg" alt=""></a>
						</div> 
					</div>
				</div>
				<div class="clearfix"></div>
			</div>


		</article>
		

		

		

		<?php if ($woning->brochures->has()): ?>

			<h2>Brochures</h2>

			<?php foreach ($woning->brochures as $brochure): ?>
				<?php $src = wp_get_attachment_url($brochure->ID); ?>
				<a href="<?php echo $src; ?>" target="_blank">download zhe brochure</a>
			<?php endforeach; ?>

		<?php endif; ?>





		<?php if ($woning->plattegronden->has()): ?>

			<h2>Plattegronden</h2>

			<?php foreach ($woning->plattegronden as $plattegrond): ?>
				<?php $src = wp_get_attachment_image_src($plattegrond->ID, 'medium'); ?>
				<img src="<?= $src[0]; ?>">
			<?php endforeach; ?>

		<?php endif; ?>

		<?php if ($woning->isOpenHuis): ?>
			Open huis van <?= $woning->openHuisVanaf ?> tot <?= $woning->openHuisTot ?>
		<?php endif; ?>


		<?php if ($woning->hasLocation): ?>
		<div class="container center">
			<h2>Kaart</h2>
			<div class="row-sm">
				<div class="col-6 left padding-sm"> 
					<div id="google-map"></div>
				</div>
				
				<div class="col-6 left padding-sm"> 
					<div id="google-streetview"></div>
				</div>
			</div>
			
		</div>
		<?php endif; ?>

		<h2>In de buurt</h2>

		<ul>
		<?php foreach ($woning->nearby('1km')->all()->without($woning)->ascending('radius') as $post): ?>
			<li><?= $woning->plaats ?> - <?= $woning->adres ?>: <?= $woning->radius ?></li>
		<?php endforeach; ?>
		</ul>

	</div>
</div>




<?php get_footer(); ?>
