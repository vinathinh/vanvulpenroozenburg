<!-- <div class="search-settings">
	Sorteren op:
	<select name="orderby" class="stylized-auto-width">
		<?php foreach (Wonen::form()->orderings() as $ordering): ?>
			<option data-text="<?= esc_attr($ordering['label']) ?> <small>(<?= esc_attr($ordering['meta']) ?>)</small>" value="<?= $ordering['orderby'] ?>" <?= selected($ordering['current']); ?>>
				<?= $ordering['label'] ?> (<?= $ordering['meta'] ?>)
			</option>
		<?php endforeach; ?>
	</select>
	<ul class="display">
		<li class="display-list"><a href="#" class="search-display" data-display="list">Lijst</a></li>
		<li class="display-grid"><a href="#" class="search-display" data-display="grid">Grid</a></li>
		<li class="display-map"><a href="#" class="search-display" data-display="map">Map</a></li>
	</ul>
	<div style="clear: right;"></div>
</div> -->

<?php if (have_posts()): ?>

	<div id="entity-items" class="results-container clear hidden-map container center">
		<?php while (have_posts()): the_post(); ?>
			<?= Wonen::template('item')->cache(); ?>
		<?php endwhile; ?>
		<div class="clearfix"></div>
	</div>

<!-- 	<div class="search-map visible-map">
		<div id="google-map"></div>
		<div class="clear"></div>
	</div> -->

	<div class="nav-previous pagination"><?php previous_posts_link('<span class="meta-nav">&larr;</span> Vorige'); ?></div>
	<div class="nav-next pagination"><?php next_posts_link('Volgende <span class="meta-nav">&rarr;</span>'); ?></div>

	<div class="nav-next pagination"><?php next_posts_link('Meer woningen'); ?></div>

<?php else: ?>
	<?= Wonen::template('none'); ?>
<?php endif; ?>

<div class="search-load-overlay"></div>
