<?php namespace Wonen;

use Carbon\Carbon;

// Register scripts
wp_register_script('wonen-archive', get_stylesheet_directory_uri().'/js/wonen/archive.js', array('custompost-liveform', 'custompost-slider', 'custompost-rangeslider', 'custompost-showmore', 'realworks-googlemap', 'selectBoxIt'), false, true);
wp_register_script('wonen-single', get_stylesheet_directory_uri().'/js/wonen/single.js', array('realworks-googlemap'), false, true);

// Thumbnail helper method
Wonen::macro('thumbnail', function($woning, $size = null)
{
	$data = wp_get_attachment_image_src(get_post_thumbnail_id($woning->id()), $size);

	return $data ? $data[0] : null;
});

// Toon koopprijs met koopconditie
Wonen::formatter('koopprijs', function($value, $field)
{
	if ($field->post()->prijsTonen->isTrue())
	{
		return $field->formatter('money').' '.$field->post()->koopconditie;
	}
	else
	{
		return 'Prijs op aanvraag';
	}
});

// Wijzig koopconditie waardes in afkortingen
Wonen::values('koopconditie', array(
	'kosten koper' => 'k.k.',
	'vrij op naam' => 'v.o.n.',
));

// Toon huurprijs met huurconditie
Wonen::formatter('huurprijs', function($value, $field)
{
	if ($field->post()->prijsTonen->isTrue())
	{
		return $field->formatter('money').' '.$field->post()->huurconditie;
	}
	else
	{
		return 'Prijs op aanvraag';
	}
});

// Open huis helper, bepaalt of de openhuis tot datum al geweest is
Wonen::macro('isOpenHuis', function($woning)
{
	return $woning->openHuisTot->date() > Carbon::now();
});

// Geef openhuis tot datum zonder datum weer als gelijk aan vanaf datum
Wonen::formatter('openHuisTot', function($value, $field)
{
	if ($field->post()->openHuisVanaf->isSameDay($value))
	{
		return $field->formatter('datetime')->render('%R');
	}
	else
	{
		return $field->formatter('datetime')->render();
	}
});
