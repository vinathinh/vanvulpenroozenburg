<div class="search-field"> 
	<div class="search-items">
		<input type="text" name="adres" value="<?= Wonen::form()->value('adres'); ?>">
	</div>
</div>

<div class="search-field show-more hide-non-selected track-selected" data-name="plaats">
	<div class="search-items">
		<?php Wonen::form()->dropdown('plaats', array(
			'emptyLabel' => 'Plaats',
			'showCounts' => true,
			'hideWhenNoResults' => false,
			'hideWhenNotExists' => true,
			'label' => array(
				// Specify the count as attribute, so items without results may be displayed differently using css
				'data-count' => '{{count}}',
			),
		)); ?>
	</div>
</div>

<div class="search-field radius">
	<div class="search-items">
		<?php Wonen::form()->dropdown('radius', array(
			'emptyLabel' => 'Straal',
			'showCounts' => true, 
			'option' => array(
				'data-text' => '{{label}} <small>({{count}})</small>'
			),
		)); ?>
	</div>
</div>

<div class="search-field">
	<div class="search-items">
		<?php Wonen::form()->dropdown('kamers', array(
			'emptyLabel' => 'Kamers',
			'showCounts' => true, 
			'option' => array(
				'data-text' => '{{label}} <small>({{count}})</small>'
			),
		)); ?>
	</div>
</div>

<div class="search-field">
	<div class="search-items">
		<?php Wonen::form()->dropdown('oppervlakte', array(
			'emptyLabel' => 'Oppervlakte',
			'showCounts' => true, 
			'option' => array(
				'data-text' => '{{label}} <small>({{count}})</small>'
			),
		)); ?>
	</div>
</div>


<div class="search-field hide-non-selected track-selected">
	<div class="search-items">
		<?php Wonen::form()->checkboxes('status', array('showCounts' => true)); ?>
	</div>
	<!-- <a href="#" class="clear-selected">Wissen</a> -->
</div>

<!-- <div class="search-field">
	<h4>Type</h4>
	<div class="search-items">
		<?php Wonen::form()->checkboxes('type', array('showCounts' => true)); ?>
	</div>
</div> -->

<div class="search-field__prices" data-min-label="Geen minimum" data-max-label="&infin;" data-label="text">
	<div class="search-items">
		<?php Wonen::form()->minDropdown('prijs', array(
			'emptyLabelMin' => 'Minimum',
			'emptyLabelMax' => 'Maximum',
			'showCounts' => true, 
			'option' => array(
				'data-text' => '{{label}} <small>({{count}})</small>'
			),
		)); ?> 
	</div>
	<div class="search-items"> 
		<?php Wonen::form()->maxDropdown('prijs', array(
			'emptyLabelMin' => 'Minimum',
			'emptyLabelMax' => 'Maximum',
			'showCounts' => true, 
			'option' => array(
				'data-text' => '{{label}} <small>({{count}})</small>'
			),
		)); ?>
	</div>
</div>

<input class="btn-blue" type="submit" value="Zoeken">
