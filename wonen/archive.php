<?php get_header(); ?>

<?php wp_enqueue_script('wonen-archive') ?>
<?php wp_localize_script('wonen-archive', 'Wonen', array(
	'maps' => Wonen::maps('all'),
)); ?>
<div class="search-map visible-map mappie">
	<div id="google-map"></div>
	<div class="clear"></div>
</div>

<section id="primary" class="search-archive">
	<div id="content" role="main">

		<div class="archive-header container center">
			<h1 class="archive-title">Zoeken in 
				<span class="search-results-total"><?= $wp_query->found_posts; ?></span>
				<!-- <span class="search-results-singular <?= _n('', 'hidden', $wp_query->found_posts) ?>">woning</span> -->
				<span class="search-results-plural <?= _n('hidden', '', $wp_query->found_posts) ?>">woningen</span>
				<!-- gevonden -->
			</h1>

			<?php if (Wonen::isArchive(array('type' => 'koop'))): ?>
				<p>Alleen koopwoningen worden getoond.</p>
			<?php elseif (Wonen::isArchive(array('type' => 'huur'))): ?>
				<p>Alleen huurwoningen worden getoond.</p>
			<?php endif; ?>

			<!-- <?php if (($new = Wonen::whereSql('datumInvoer', '> DATE_SUB(NOW(), INTERVAL 1 YEAR)')->count()) > 0): ?>
			<p>
				<?php if ($new === 1): ?>
				Er is een nieuwe woning toegevoegd in het laatste jaar.
				<?php else: ?>
				Er zijn <?= $new; ?> nieuwe woningen toegevoegd in het laatste jaar.
				<?php endif; ?>
			</p>
			<?php endif; ?> -->
		</div>

		<div class="archive-container clear">

			<form id="entity-search-form" method="GET" action="">
				<div id="entity-search" class="search-sidebar container center"> 
					<?= Wonen::template('search'); ?>
				</div>

				<div id="entity-results" class="search-content <?= array_get($_COOKIE, 'display', 'list'); ?>">
					<?= Wonen::template('loop'); ?>
				</div>
			</form>
			<script type="text/javascript">
			// Manully add a loading overview if a custom search page is to be loaded, to avoid flickering at load
			if (location.hash) document.getElementById('entity-search-form').className = 'search-loading';
			</script>

		</div>

	</div>
</section>

<?php get_footer(); ?>
