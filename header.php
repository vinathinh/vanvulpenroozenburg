<!doctype html>

<html>
<head>
	<title></title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"> 

	<?php wp_head(); ?>
</head>
<body>

<header>
	<div class="container center">
		<nav>
			<a href="#" class="logo">
				<img src="<?php echo get_template_directory_uri() ?>/img/LogoHorizontaalWit.png" alt="">
			</a>
			
			<?php
				wp_nav_menu( array(
			        'theme_location' => 'primary',
			        'menu_class'     => 'primary-menu',
		    	) ); 
	    	?>
		</nav>
	</div>
	
</header>