console.log("%cSite by vinathinh.com",'color: #1a237e; font-size: 32px; font-family: "Arial"; font-weight: bold; text-transform: uppercase;');

// external js: flickity.pkgd.js

var $carousel = $('.carousel').flickity({
	prevNextButtons: false,
	pageDots: false,
	wrapAround: true
});



var meer = document.querySelector('.single__omschrijving--toggle'),
	meer2 = document.querySelector('.single__omschrijving--content');

if (meer != null) {
  meer.addEventListener('click', function() {
    meer2.classList.toggle('filter-is-open')
  });
  $carousel.css({ opacity: 1 });

var $carouselNav = $('.carousel-nav');
var $carouselNavCells = $carouselNav.find('.carousel-cell');

$carouselNav.on( 'click', '.carousel-cell', function( event ) {
  var index = $( event.currentTarget ).index();
  $carousel.flickity( 'select', index );
});

var flkty = $carousel.data('flickity');
var navTop  = $carouselNav.position().top;
var navCellHeight = $carouselNavCells.height();
var navHeight = $carouselNav.height();

$carousel.on( 'select.flickity', function() {
  // set selected nav cell
  $carouselNav.find('.is-nav-selected').removeClass('is-nav-selected');
  var $selected = $carouselNavCells.eq( flkty.selectedIndex )
    .addClass('is-nav-selected');
  // scroll nav
  var scrollY = $selected.position().top +
    $carouselNav.scrollTop() - ( navHeight + navCellHeight ) / 2;
  $carouselNav.animate({
    scrollTop: scrollY
  });
});
} 


