<?php
/**
 * Template Name: Home
 */
?>
<?php get_header(); ?>
<form target="_blank" id="entity-search-form" method="GET" action="/vanvulpenroozenburg/woningen/">
	<div id="entity-search" class="search-sidebar">
		<?= Wonen::template('search-frontpage'); ?>
	</div>
</form>
<script type="text/javascript">
// Manully add a loading overview if a custom search page is to be loaded, to avoid flickering at load
if (location.hash) document.getElementById('entity-search-form').className = 'search-loading';
</script>

 
 

<?php 
    query_posts(array( 
        'post_type'=> 'realworks_wonen',
        'showposts' => 3 
    ) );  
?>
<?php while (have_posts()) : the_post(); ?>
        <article class="woning col-4 left">
	<div class="item">
		<a href="<?php the_permalink(); ?>" class="thumbnail">
			<?php the_post_thumbnail('medium'); ?>
		</a>

		<div class="item-content">
			<div>
				<?php the_title('<h1><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h1>'); ?>
			</div>

			<div class="entry-content hidden-sm">
				<?= $woning->listing('prijs', 'woning.gebruiksoppervlakteWoonfunctie', 'woning.aantalKamers')->item('<div>#{value}</div>'); ?>
			</div>
		</div>

		<a href="<?php the_permalink(); ?>" class="overlay"></a>
	</div>
</article>
<?php endwhile;wp_reset_query();?>
 

<?php include 'includes/module.php'; ?>	

<?php get_footer(); ?>